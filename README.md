# Description

This project builds a version of `docker:dind` except this version uses a base of Ubuntu instead of Alpine Linux.

# Usage

## Ubuntu 14.04 Docker in Docker
Run with: `docker run --privileged -t -i ubuntu-dind:14.04`

## Ubuntu 16.04 Docker in Docker
Run with: `docker run --privileged -t -i ubuntu-dind:16.04`